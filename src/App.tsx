import React, { Suspense, useState } from 'react';
import { NavBar } from './components/NavBar';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import UiKitDemoPage from './pages/UiKitDemoPage';
import { DemoUncontrolledForm } from './pages/DemoUncontrolledForm';
import { HomePage } from './pages/HomePage';
import { Page404 } from './pages/404Page';
import { UserDetails } from './pages/UserDetails';
import CrudExample from './pages/crud/CrudExample';
import { ContextDemo } from './pages/ContextDemo';
import { DrillingPropsDemo } from './pages/DrillingPropsDemo';
import { ZustandDemo } from './pages/ZustandDemo';
import { ValtioDemo } from './pages/ValtioDemo';
import create from 'zustand';
import axios from 'axios';

const UsersPage = React.lazy(() => import('./pages/UsersPage'))
const DemoControlledForm = React.lazy(() => import('./pages/DemoControlledForm'))

interface Config {
  lang: 'it' | 'en';
  theme: 'dark' | 'light'
}

export const AppContext = React.createContext<Config | null>(null)

interface AppState {
  language: string;
  token: string | null;
  changeLanguage: (lang: string) => void;
  login: (u: string, p: string) => void;
  logout: () => void;
}
// zustand state
export const store = create<AppState>((set) => ({
  language: 'it',
  token: null,
  changeLanguage: (language: string) => set({ language } ),
  login: (u: string, p: string) => {
    axios.get<{ token: string }>(`http://localhost:3001/login?user=${u}&pass=${p}`)
      .then(res => {
        set({ token: res.data.token})
      })
  },
  logout: () => {
    set({ token: null })
  }
}))

const App = () => {
  const [config, setConfig] = useState<Config>({ lang: 'it', theme: 'light' })

  function changeLang(lang: 'it' | 'en') {
    setConfig({...config, lang: lang})
  }
  return (
    <BrowserRouter>
      <AppContext.Provider value={config}>
      <NavBar />
      <hr/>
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="uikit" element={<UiKitDemoPage />} />
        <Route path="crud" element={<CrudExample />} />
        <Route path="drillingprops" element={<DrillingPropsDemo />} />
        <Route path="zustand" element={<ZustandDemo />} />
        <Route path="valtio" element={<ValtioDemo />} />
        <Route path="context" element={<ContextDemo onChangeLang={changeLang} />} />
        <Route path="users" element={
          <Suspense fallback={<div>loading....</div>}>
            <UsersPage />
          </Suspense>
        } />
        <Route path="user/:userId" element={<UserDetails />} />
        <Route path="form-controlled" element={
          <Suspense fallback={<div>loading....</div>}>
            <DemoControlledForm />
          </Suspense>
        } />
        <Route path="form-uncontrolled" element={<DemoUncontrolledForm />} />
        <Route path="404" element={<Page404 />} />
        <Route path="*" element={
          <Navigate to="404" />
        }></Route>
      </Routes>


        <footer>

        </footer>
      {/*<Routes value={page} />*/}
      </AppContext.Provider>
    </BrowserRouter>
  )
}
export default App;




