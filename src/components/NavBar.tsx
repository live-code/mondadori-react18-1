import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';
import { AppContext, store } from '../App';

export const NavBar = () => {

  const config = useContext(AppContext)
  const changeLanguage = store(state => state.changeLanguage)
  const logout = store(state => state.logout)

  const getStyles = (obj: { isActive: boolean }) => {
    return obj.isActive ? { color: 'orange'} : {}
  }

  return <div className="p-3">
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand" >
        Logo
        <button onClick={() => changeLanguage('it')}>it</button>
        <button onClick={() => changeLanguage('en')}>en</button>
      </div>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              to="users"
              className="nav-link"
              style={getStyles}
            >Users</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              to="drillingprops"
              className="nav-link"
              style={getStyles}
            >Drillling Props</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              to="context"
              className="nav-link"
              style={getStyles}
            >Context</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              to="crud"
              className="nav-link"
              style={getStyles}
            >Crud</NavLink>
          </li>
         <li className="nav-item">
            <NavLink
              to="uikit" className="nav-link"
              style={getStyles}
            >uikit</NavLink>
          </li>
         <li className="nav-item">
            <NavLink
              to="form-controlled" className="nav-link"
              style={getStyles}
            >Controlled</NavLink>
          </li>
         <li className="nav-item">
          <NavLink
            to="form-uncontrolled" className="nav-link"
            style={getStyles}
          >Uncontrolled</NavLink>
        </li>
          <li className="nav-item">
            <NavLink
              to="zustand" className="nav-link"
              style={getStyles}
            >Zustand</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              to="valtio" className="nav-link"
              style={getStyles}
            >Valtio</NavLink>
          </li>
          <li className="nav-item" onClick={logout}>
            <div className="nav-link">Quit</div>
          </li>
        </ul>
      </div>
    </nav>
  </div>
}
