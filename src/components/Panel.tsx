import React, { PropsWithChildren } from 'react';
import cn from 'classnames';

interface PanelProps {
  title: string;
  headerCls?: string;
  type?: 'success' | 'failed';
  icon?: string;
  btnLabel?: string;
  onIconClick?: () => void;
  onBtnClick?: () => void;
}
export const Panel = (props: PropsWithChildren<PanelProps>) => {

  return <div className="card">
    <div
      className={cn(
        'card-header',
        props.headerCls,
        {
          'bg-danger': props.type === 'failed',
          'bg-success': props.type === 'success',
        }
      )}
    >
      {props.title}

      {
        props.icon && (
          <div className="pull-right">
            <i className={props.icon}
               onClick={props.onIconClick}></i>
          </div>
        )
      }

    </div>
    <div className="card-body">{props.children}</div>

    {
      props.btnLabel &&
        <div className="card-footer">
          <button
            className="btn btn-primary"
            onClick={props.onBtnClick}
          >
            {props.btnLabel}
          </button>
      </div>
    }
  </div>
}
