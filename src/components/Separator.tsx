import React from 'react';

interface SeparatorProps {
  size: 'sm' | 'md' | 'xl';
  color: string
}

export const Separator = (props: SeparatorProps) => {
  function getMargins() {
    switch (props.size) {
      case 'sm': return 10;
      case 'md': return 30;
      case 'xl': return 50;
    }
  }
  function getStyles() {
    return {
      backgroundColor: props.color,
      height: 2,
      margin: getMargins()
    }
  }
  return <div style={getStyles()}></div>
}

