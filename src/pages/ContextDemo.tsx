import React, { useContext, useState } from 'react';
import { AppContext } from '../App';

const DataContext = React.createContext<number | null>(null)

export function ContextDemo(props: any) {
  const [count, setCount] = useState<number>(10)
  const [users, setUsers] = useState<number[]>([1, 2, 3])

  return <DataContext.Provider value={count}>
    context
    <Widget onIncrement={() => setCount(s => s + 1)}></Widget>

    <button onClick={() => props.onChangeLang('it')}>IT</button>
    <button onClick={() => props.onChangeLang('en')}>ENG</button>
  </DataContext.Provider>
}


export function Widget(props: { onIncrement: () => void }) {
  return <div className="card">
    <div className="card-header">WIDGET: users</div>
    <div className="card-body">
      <List onIncrement={props.onIncrement}></List>
    </div>
  </div>
}

export function List(props: { onIncrement: () => void }) {
  const state = useContext(DataContext)
  const config = useContext(AppContext)
  console.log('render list', state)
  return <ul>
    <h1>Lista: {state} - {config?.theme}</h1>
    {
      [].map(u => {
        return <li key={u}>{u}</li>
      })
    }

    <button onClick={() => props.onIncrement()}>+</button>
  </ul>
}
