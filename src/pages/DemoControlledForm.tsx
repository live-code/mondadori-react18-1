import { FormEvent, useEffect, useState } from 'react';
import classNames from 'classnames';


interface FormData {
  name: string;
  city: string;
  phone: string;
  subscription: boolean,
  gender: 'M' | 'F' | '',
}

export default function DemoControlledForm() {
  const [formData, setFormData] = useState<FormData>({ name: '', city: '', phone: '', gender: '', subscription: false});
  const [dirty, setDirty] = useState<boolean>(false)

  useEffect(() => {
    // simulate populate form
    setTimeout(() => {
      const res: FormData = {  name: 'pippo', city: 'trieste', phone: '123', subscription: true, gender: 'M'}
      setFormData(res);
    }, 1000)
  }, [])

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    setDirty(true);
    const key = e.currentTarget.name;
    const value = e.currentTarget.type === 'checkbox' ?
                      (e.currentTarget as HTMLInputElement).checked : e.currentTarget.value;
    setFormData({  ...formData, [key]: value })
  }

  function save(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    console.log('SAVE', formData)
  }

  const isNameValid = formData.name.length > 3;
  const isCityValid = formData.city.length ;
  const isFormValid = isNameValid && isCityValid;

  return <form onSubmit={save}>

    {JSON.stringify(formData)}

    <hr/>
    { (!isNameValid && dirty) && <small>name requires at least 4 chars</small>}
    <input
      className={classNames(
        'form-control', { 'is-invalid': !isNameValid && dirty, 'is-valid': isNameValid}
      )}
      type="text" placeholder="name"
      value={formData.name}
      onChange={onChangeHandler}
      name="name"
    />

    <input
      type="text" placeholder="city"
      className={classNames(
        'form-control', { 'is-invalid': !isCityValid && dirty, 'is-valid': isCityValid}
      )}
      value={formData.city}
      onChange={onChangeHandler}
      name="city"
    />

    <input
      type="text" placeholder="phone"
      className="form-control"
      value={formData.phone}
      onChange={onChangeHandler}
      name="phone"
    />

    <select value={formData.gender}  onChange={onChangeHandler} name="gender">
      <option value="">Select Gender</option>
      <option value="M">Male</option>
      <option value="F">Female</option>
    </select>

    <input type="checkbox" name="subscription"
           checked={formData.subscription}
           onChange={onChangeHandler}/> subscribe

    <hr/>
    <button type="submit" disabled={!isFormValid}>save</button>
  </form>
}
