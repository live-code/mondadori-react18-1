import { useEffect, useRef, useState } from 'react';
import { Simulate } from 'react-dom/test-utils';

export function DemoUncontrolledForm() {
  const inputNameEl = useRef<HTMLInputElement>(null);   // { current: reference }
  const inputCityEl = useRef<HTMLInputElement>(null);   // { current: reference }

  useEffect(() => {
    // populate form
    // const userForm = { name: 'pippo', city: 'trieste'};
    // inputNameEl.current && (inputNameEl.current.value = userForm.name);
  }, [])

  function onKeyUpHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter' && inputNameEl.current?.value) {
      console.log( '-->', inputNameEl.current?.value , inputCityEl.current?.value)
      inputNameEl.current.value = '';
      inputCityEl.current && (inputCityEl.current.value = '')
    }
  }

  return <div>
    <h1>Demo uncontrolled</h1>
    <input ref={inputNameEl} type="text" onKeyUp={onKeyUpHandler} placeholder="name"/>
    <input ref={inputCityEl} type="text" onKeyUp={onKeyUpHandler} placeholder="city"/>
  </div>
}
