import { useState } from 'react';

// esempio con 3 componenti e passaggio di proprietà
export function DrillingPropsDemo() {
  const [users, setUsers] = useState<number[]>([1, 2, 3])
  return <div>
    context
    <Widget users={users}></Widget>
  </div>
}


// ----widget.tsx
interface WidgetProps {
  users: number[];
}
export function Widget(props: WidgetProps) {
  return <div className="card">
    <div className="card-header">WIDGET: {props.users.length} users</div>
    <div className="card-body">
      <List users={props.users}></List>
    </div>
  </div>
}


// ----- list.tsx
interface ListProps {
  users: number[];
}
export function List(props: ListProps) {
  return <ul>
    {
      props.users.map(u => {
        return <li key={u}>{u}</li>
      })
    }
  </ul>
}
