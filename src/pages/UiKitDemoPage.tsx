import { Panel } from '../components/Panel';
import { Separator } from '../components/Separator';
import React from 'react';

const UiKitDemoPage = () => {
  function gotoUrl() {
    window.open('http://www.facebook.com')
  }
  function doSomethingElse() {
    window.alert('yooooo!')
  }

  return (
    <div className="m-3">
      <Panel
        title="Facebook Profile"
        icon="fa fa-facebook"
        btnLabel="GO TO FACEBOOK"
        onIconClick={gotoUrl}
        onBtnClick={gotoUrl}
      >
        my facebook data ...
      </Panel>

      <Separator size="sm" color="cyan" />

      <Panel
        title="Google Profile"
        icon="fa fa-google"
        onIconClick={doSomethingElse}
      >
        my google data ...
      </Panel>


      <Separator size="sm" color="cyan" />

      <Panel title="pluto">
        <h1>pippo</h1>
        <input />
        <input />
        <button>CLICK</button>
      </Panel>

      <Panel title="OK!!" type="success" icon="fa fa-times">dati salvati db</Panel>
      <Panel
        title="OK!!" headerCls="bg-dark text-white"
        btnLabel="DO SOMETHING"
        onBtnClick={function() {
          console.log('do something')
        }}
      >bla bla</Panel>

    </div>
  )
}

export default UiKitDemoPage;
