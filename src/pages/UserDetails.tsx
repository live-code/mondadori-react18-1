import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { User } from '../model/user';
import classNames from 'classnames';
import create from 'zustand';
import { store } from '../App';

export const userDetailsStore = create<{ counter: number, inc: () => void }>(set => ({
  counter: 123,
  inc: () => {
    set(state => ({ counter: state.counter + 1}))
  }
}))

export function UserDetails() {
  const [user, setUser] = useState<Omit<User, 'id'>>({  name: '', gender: '' })
  const params = useParams();

  useEffect(() => {
    axios.get('http://localhost:3001/users/' + params.userId)
      .then(res => {
        setUser(res.data)
      })

  }, [])

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
     setUser({ ...user, name: e.currentTarget.value })
  }

  function save() {
    axios.patch('http://localhost:3001/users/' + params.userId, user)
      .then(() => {
        window.alert('done')
      })

  }

  return <div>
    <h1>{user?.name}</h1>
    <input type="text" value={user?.name} onChange={onChangeHandler}/>
    <button onClick={save}>save</button>

    <i className={classNames(
      'fa fa-4x',
      {
        'fa-venus': user?.gender === 'F',
        'fa-mars': user?.gender === 'M',
      }
    )}></i>

    <ChildComponent />

  </div>
}

function ChildComponent() {
  const token = store(state => state.token)
  const counter = userDetailsStore(state => state.counter)
  const inc = userDetailsStore(state => state.inc)
  return <h1 onClick={inc}>
    child {counter} -{token}
  </h1>
}
