import { useEffect, useState } from 'react';
import { User } from '../model/user';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';

const UsersPage = () => {
  const [users, setUsers] = useState<User[]>([])
  const [error, setError] = useState<boolean>(false)
  const navigate = useNavigate();

  useEffect(() => {
    setTimeout(() => {
      axios.get<User[]>('http://localhost:3001/users')
        .then(res => {
          setUsers(res.data)
        })
    }, 400)

  }, [])

  function addUser(e: React.KeyboardEvent<HTMLInputElement>) {
    setError(false)

    if (e.key === 'Enter') {
      axios.post<User>(
        'http://localhost:3001/users',
        { name: e.currentTarget.value}
      )
        .then(res => setUsers([...users, res.data]))
        .catch(() => setError(true))
    }
  }

  function deleteUser(id: number, e: React.MouseEvent) {
    e.stopPropagation();
    setError(false)
    axios.delete(`http://localhost:3001/users/${id}`)
      .then(() => {
        const newUsers = users.filter(u => u.id !== id)
        setUsers(newUsers)
      })
      .catch(() => {
        setError(true)
      })

  }

  function gotoUserPage(id: number) {
    navigate('/user/' + id)
  }

  return  <div>
    { error && <div className="alert alert-danger">AHIA!</div>}

    <input type="text" onKeyUp={addUser} placeholder="Add user name"/>
    {/*<button onClick={addUser}>ADD USER</button>*/}
    <br/>
    <br/>
    {
      users.map(u => {
        return (
          <li key={u.id} className="list-group-item"
              onClick={() => gotoUserPage(u.id)}>
            {u.name} {u.id}
            <div className="pull-right">
              <i className="fa fa-trash"
                 onClick={(e) => deleteUser(u.id, e)}></i>
            </div>
          </li>
        )
      })
    }
  </div>
}

function Panel(props: any) {
  return <div className="alert alert-success">
    {props.title}
  </div>
}



/*


<MyPanelReact
  title="ciao"
  icon="♥️"
  opened={true}
  onIconClick={() => console.log('clicked')}
>
  efwefwe
</MyPanelReact>*/

export default UsersPage
