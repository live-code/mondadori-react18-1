import { useState } from 'react';
import { proxy, useSnapshot } from 'valtio';

const store = proxy<{ count: number, text: string}>({
  count: 0, text: 'hello'
})

export function ValtioDemo() {
  return <div>
    <h1>Valtio demo</h1>
    <Widget></Widget>
  </div>
}


export function Widget() {
  return <div className="card">
    <div className="card-header">WIDGET:</div>
    <div className="card-body">
      <List></List>
    </div>
  </div>
}


export function List() {
  const state = useSnapshot(store);

  return <h1>
    <button onClick={() => store.count = store.count + 1}> {state.count}</button>
  </h1>
}
