import { useState } from 'react';
import { store } from '../App';

export function ZustandDemo() {
  const login = store(state => state.login)

  return <div>
    context
    <Widget></Widget>
  <hr/>
    <input type="text"/>
    <input type="text"/>
    <button onClick={() => login('fabio','123')}>Login</button>
  </div>
}


export function Widget() {
  return <div className="card">
    <div className="card-header">WIDGET: </div>
    <div className="card-body">
      <List></List>
    </div>
  </div>
}


export function List() {
  console.log('list render')
  const language = store(state => state.language)
  const token = store(state => state.token)
  return <ul>
    <h1>Lista</h1>
    <h1>token</h1>
  </ul>
}
