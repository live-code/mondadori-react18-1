import React, { useEffect, useState } from 'react';
import { Utente } from './model/utente';
import axios from 'axios';
import { UsersList } from './components/UsersList';
import { UserForm } from './components/UserForm';
import { useCrud } from './hooks/useCrud';

export default function CrudExample() {
  // custom Hooks
  const {
    utenti, formData, actions
  } = useCrud();

  return (<>

    <div className="row">
      <div className="col">
        <UserForm
          activeUser={formData}
          onSave={actions.save}
          onReset={actions.onReset}
          onUpdateActiveUser={obj => actions.setFormData(state => ({...state, ...obj}))}
        />
      </div>

      <div className="col">
        <UsersList
          data={utenti}
          activeUser={formData}
          onSelectUser={actions.selectUtente}
          onDeleteUser={actions.deleteUtente}
        />
      </div>
    </div>
  </>)
}
