import classNames from 'classnames';
import React, { useState } from 'react';
import { Utente } from '../model/utente';

interface UserFormProps {
  activeUser: Partial<Utente>;
  onSave: () => void;
  onReset: () => void;
  onUpdateActiveUser: (obj: { [key: string]: string | boolean }) => void;
}
export function UserForm(props: UserFormProps) {
  const [dirty, setDirty] = useState<boolean>(false)


  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    setDirty(true);
    const key = e.currentTarget.name;
    const value = e.currentTarget.type === 'checkbox' ?
      (e.currentTarget as HTMLInputElement).checked : e.currentTarget.value;

    props.onUpdateActiveUser({ [key]: value })
  }


  function save(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSave();
  }


  const isNameValid = props.activeUser.name && props.activeUser.name.length > 3;
  const isCityValid = props.activeUser.city && props.activeUser.city.length ;
  const isFormValid = isNameValid && isCityValid;


  return (
    <form onSubmit={save}>

      {/*{JSON.stringify(props.activeUser)}*/}

      <hr/>
      { (!isNameValid && dirty) && <small>name requires at least 4 chars</small>}
      <input
        className={classNames(
          'form-control', { 'is-invalid': !isNameValid && dirty, 'is-valid': isNameValid}
        )}
        type="text" placeholder="name"
        value={props.activeUser.name}
        onChange={onChangeHandler}
        name="name"
      />

      <input
        type="text" placeholder="city"
        className={classNames(
          'form-control', { 'is-invalid': !isCityValid && dirty, 'is-valid': isCityValid}
        )}
        value={props.activeUser.city}
        onChange={onChangeHandler}
        name="city"
      />

      <input
        type="text" placeholder="phone"
        className="form-control"
        value={props.activeUser.phone}
        onChange={onChangeHandler}
        name="phone"
      />

      <select value={props.activeUser.gender}  onChange={onChangeHandler} name="gender">
        <option value="">Select Gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>

      <input type="checkbox" name="subscription"
             checked={props.activeUser.subscription}
             onChange={onChangeHandler}/> subscribe

      <hr/>
      <button type="submit" disabled={!isFormValid}>
        { props.activeUser.id ? 'EDIT' : 'ADD' }
      </button>
      <button type="button" onClick={props.onReset}>reset</button>
    </form>

  )
}
