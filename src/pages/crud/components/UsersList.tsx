import classNames from 'classnames';
import React from 'react';
import { Utente } from '../model/utente';

interface UsersListProps {
  data: Utente[];
  activeUser: Partial<Utente>;
  onSelectUser: (u: Utente) => void;
  onDeleteUser: (idToDelete: number) => void;
}

export function UsersList(props: UsersListProps) {
  function deleteUtente(idToDelete: number, e: React.MouseEvent) {
    e.stopPropagation();
    props.onDeleteUser(idToDelete);
  }

    return (
    <ul className="list-group">
      {
        props.data.map(u => {
          return <li
            key={u.id}
            className={classNames('list-group-item ', {active: u.id === props.activeUser.id})}
            onClick={() => props.onSelectUser(u)}
          >
            {u.name} {u.id}
            <i className="fa fa-trash"
               onClick={(e) => deleteUtente(u.id, e)}></i>
          </li>
        })
      }
    </ul>

  )
}
