import { useEffect, useState } from 'react';
import { Utente } from '../model/utente';
import axios from 'axios';

const initialState: Omit<Utente, 'id'> = { name: '', city: '', phone: '', gender: '', subscription: false};


export function useCrud() {
  const [formData, setFormData] = useState<Partial<Utente>>(initialState);
  const [utenti, setUtenti] = useState<Utente[]>([])

  useEffect(() => {
    axios.get<Utente[]>('http://localhost:3001/utenti')
      .then(res => setUtenti(res.data))
  }, [])


  function onSaveHandler() {
    if (formData.id) {
      edit()
    } else {
      add()
    }
  }

  function edit() {
    axios.patch<Utente>('http://localhost:3001/utenti/' + formData.id, formData)
      .then(res => {
        setUtenti(
          utenti.map(u => u.id === formData.id ? res.data : u)
        )
      })
  }



  function add() {
    axios.post<Utente>('http://localhost:3001/utenti', formData)
      .then(res => {
        setUtenti([...utenti, res.data]);
        setFormData(initialState)
        //setDirty(false)
      })
  }


  function deleteUtente(idToDelete: number) {
    axios.delete('http://localhost:3001/utenti/' + idToDelete)
      .then(res => {
        setUtenti(
          utenti.filter(u => u.id !== idToDelete)
        );

        if (idToDelete === formData.id) {
          setFormData(initialState)
        }
      })
  }

  function onResetHandler() {
    setFormData(initialState)
  }

  function selectUtente(u: Utente) {
    setFormData(u);
  }

  return {
    utenti,
    formData,
    actions: {
      selectUtente,
      onReset: onResetHandler,
      deleteUtente,
      add,
      save: onSaveHandler,
      setFormData
    }
  }
}
