
export interface Utente {
  id: number;
  name: string;
  city: string;
  phone: string;
  subscription: boolean,
  gender: 'M' | 'F' | '',
}
